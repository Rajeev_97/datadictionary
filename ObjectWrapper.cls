
global with sharing class ObjectWrapper implements Comparable {
    @AuraEnabled public String objectLabel;
    @AuraEnabled public String objectName;

    public ObjectWrapper(String objectLabel, String objectName) {
        this.objectLabel = objectLabel;
        this.objectName = objectName;
    }

    global Integer compareTo(Object compareTo) {
        ObjectWrapper compareToObject = (ObjectWrapper) compareTo;
        
        Integer returnValue = 0;

        if (objectLabel > compareToObject.objectLabel) {
            returnValue = 1;
        } else if (objectLabel < compareToObject.objectLabel) {
            returnValue = -1;
        }
        return returnValue;       
    }
}
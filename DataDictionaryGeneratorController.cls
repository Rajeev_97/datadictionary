public with sharing class DataDictionaryGeneratorController {

    private static final String SEPARATOR = ',';
    private static final String NEW_LINE = '\n';
    private static Map<String, Schema.SObjectType> globalDescribe {
        get {
            if (globalDescribe == null) {
                globalDescribe = Schema.getGlobalDescribe();
            }
            return globalDescribe;
        }
        set;
    }

    private static Map<Schema.DisplayType, String> displayTypeToUiTypeMap {
        get {
            if (displayTypeToUiTypeMap == null) {
                displayTypeToUiTypeMap = new Map<Schema.DisplayType, String>();
                displayTypeToUiTypeMap.put(Schema.DisplayType.ADDRESS, 'Address');
                displayTypeToUiTypeMap.put(Schema.DisplayType.ANYTYPE, 'AnyType');
                displayTypeToUiTypeMap.put(Schema.DisplayType.BASE64, 'Base64');
                displayTypeToUiTypeMap.put(Schema.DisplayType.BOOLEAN, 'Checkbox');
                displayTypeToUiTypeMap.put(Schema.DisplayType.COMBOBOX, 'Combobox');
                displayTypeToUiTypeMap.put(Schema.DisplayType.CURRENCY, 'Currency');
                displayTypeToUiTypeMap.put(Schema.DisplayType.DATACATEGORYGROUPREFERENCE, 'DataCategoryGroupReference');
                displayTypeToUiTypeMap.put(Schema.DisplayType.DATE, 'Date');
                displayTypeToUiTypeMap.put(Schema.DisplayType.DATETIME, 'DateTime');
                displayTypeToUiTypeMap.put(Schema.DisplayType.DOUBLE, 'Number');
                displayTypeToUiTypeMap.put(Schema.DisplayType.EMAIL, 'Email');
                displayTypeToUiTypeMap.put(Schema.DisplayType.ENCRYPTEDSTRING, 'Text(Encrypted)');
                displayTypeToUiTypeMap.put(Schema.DisplayType.ID, 'ID');
                displayTypeToUiTypeMap.put(Schema.DisplayType.INTEGER, 'Number');
                displayTypeToUiTypeMap.put(Schema.DisplayType.LONG, 'Number');
                displayTypeToUiTypeMap.put(Schema.DisplayType.MULTIPICKLIST, 'Multi-Picklist');
                displayTypeToUiTypeMap.put(Schema.DisplayType.PERCENT, 'Percent');
                displayTypeToUiTypeMap.put(Schema.DisplayType.PHONE, 'Phone');
                displayTypeToUiTypeMap.put(Schema.DisplayType.PICKLIST, 'Picklist');
                displayTypeToUiTypeMap.put(Schema.DisplayType.REFERENCE, 'Reference');
                displayTypeToUiTypeMap.put(Schema.DisplayType.STRING, 'Text');
                displayTypeToUiTypeMap.put(Schema.DisplayType.TEXTAREA, 'Text Area');
                displayTypeToUiTypeMap.put(Schema.DisplayType.TIME, 'Time');
                displayTypeToUiTypeMap.put(Schema.DisplayType.URL, 'Url');
            }
            return displayTypeToUiTypeMap;
        }
        set;
    }

    public static Set<String> skippedObjects {
        get {
            if (skippedObjects == null) {
                skippedObjects = new Set<String>();
                skippedObjects.add('AuthorizationForm');
                skippedObjects.add('AuthorizationFormConsent');
                skippedObjects.add('AuthorizationFormDataUse');
                skippedObjects.add('AuthorizationFormText');
                skippedObjects.add('UserAppMenuCustomization');
                skippedObjects.add('UserProvisioningRequest');
                skippedObjects.add('UserProvisioningLog');
                skippedObjects.add('UserProvMockTarget');
                skippedObjects.add('UserProvAccount');
                skippedObjects.add('UserProvAccountStaging');
            }
            return skippedObjects;
        }
        set;
    }

    @AuraEnabled
    public static List<ObjectWrapper> getObjects() {
        List<ObjectWrapper> objectDetails = new List<ObjectWrapper>();
        for (Schema.SObjectType objectType : globalDescribe.values()) {
            Schema.DescribeSObjectResult objectResult = objectType.getDescribe();
            if (isValidObject(objectResult)) {
                objectDetails.add(new ObjectWrapper(objectResult.getLabel(), objectResult.getName()));
            }
        }
        objectDetails.sort();
        return objectDetails;
    }


    @AuraEnabled
    public static String generateCsvString(List<String> listOfObjects){

        String csvString = '"Object Label"' + SEPARATOR
                            + '"Object Api Name"' + SEPARATOR
                            + '"Field Label"' + SEPARATOR
                            + '"Field API Name"' + SEPARATOR
                            + '"Standard/Custom"' + SEPARATOR
                            + '"Type"' + SEPARATOR
                            + '"Help Text"' + SEPARATOR
                            + '"Picklist Values"' + SEPARATOR
                            + '"External Id"' + SEPARATOR
                            + '"Unique"' + SEPARATOR
                            + '"Required"' + NEW_LINE;
        
        for (String objectName : listOfObjects) {
            Schema.SObjectType objectType = globalDescribe.get(objectName);
            if (objectType != null) {
                Schema.DescribeSObjectResult objectResult = objectType.getDescribe();
                for (Schema.SObjectField objectField : objectResult.fields.getMap().values()) {
                    Schema.DescribeFieldResult fieldResult = objectField.getDescribe();
                    //Object Label
                    csvString += '"' + objectResult.getLabel() + '"' + SEPARATOR; 
                    //Object Name
                    csvString += '"' + objectResult.getName() + '"' + SEPARATOR; 
                    //Field Label
                    csvString += '"' + fieldResult.getLabel() + '"' + SEPARATOR;
                    //Field API Name
                    csvString += '"' + fieldResult.getName() + '"' + SEPARATOR;
                    //Standard / Custom
                    csvString += '"' + (fieldResult.isCustom() ? 'Custom' : 'Standard') + '"' + SEPARATOR;
                    //Field Type
                    csvString += '"' + getFieldType(fieldResult) + '"' + SEPARATOR;
                    //Help text
                    csvString += '"' + (String.isNotBlank(fieldResult.getInlineHelpText()) ?  fieldResult.getInlineHelpText() : '') + '"' + SEPARATOR;
                    //Picklist Values
                    csvString += '"' + processPicklistValues(fieldResult.getPicklistValues()) + '"' + SEPARATOR;
                    //External Id field
                    csvString += '"' + fieldResult.isExternalID() + '"' + SEPARATOR;
                    //External Id field
                    csvString += '"' + fieldResult.isUnique() + '"' + SEPARATOR;
                    //Required Field
                    csvString += '"' + (!fieldResult.isNillable()) + '"' + NEW_LINE;
                }
            }
        }
        return csvString;

    }

    private static String getFieldType(Schema.DescribeFieldResult fieldResult) {
        String displayType = displayTypeToUiTypeMap.get(fieldResult.getType());
        if (displayType == 'Reference') {
            if (fieldResult.getRelationshipOrder() != null) {
                displayType = 'Master-Detail(';
            } else {
                displayType = 'Lookup(';
            }
            
            List<Schema.sObjectType> parentObjects = fieldResult.getReferenceTo();
            for (Schema.sObjectType parentObject : parentObjects) {
                displayType += parentObject.getDescribe().getLabel() + ',';
            }
            displayType = displayType.removeEnd(',');
            displayType += ')';
        } else if (displayType == 'Currency' || displayType == 'Number' || displayType == 'Percent') {
            displayType += '(' + (fieldResult.getPrecision() - fieldResult.getScale()) + ',' + fieldResult.getScale() + ')';
        } else if (displayType == 'Text Area' && fieldResult.isHtmlFormatted()) {
            displayType = 'Rich Text Area';
        }

        if (displayType == 'Text' || displayType == 'Text Area' || displayType == 'Text (Encrypted)') {
            displayType += '(' + fieldResult.getLength() + ')';
        }

        if (fieldResult.isCalculated()) {
            displayType = 'Formula(' + displayType + ')';
        }

        if (fieldResult.isAutoNumber()) {
            displayType = 'AutoNumber';
        }

        return displayType;
    }


    private static String processPicklistValues(List<Schema.PicklistEntry> picklistValues) {
        String picklistAsString = '';
        if (picklistValues != null) {
            for (Schema.PicklistEntry picklistValue : picklistValues) {
                if (picklistValue.isActive()) {
                    picklistAsString += picklistValue.getValue() + NEW_LINE;
                }
            }
        }
        return picklistAsString.removeEndIgnoreCase(NEW_LINE);
    }



    private static Boolean isValidObject(Schema.DescribeSObjectResult objectResult) {
        Boolean isValid = true;
        if (!objectResult.isCreateable()) {
            isValid = false;
        }
        if (!objectResult.isUpdateable()) {
            isValid = false;
        }
        if (objectResult.getName().endsWith('Share')) {
            isValid = false;
        }
        if (objectResult.getName().endsWith('OwnerSharingRule')) {
            isValid = false;
        } 
        if (objectResult.getName().endsWith('History')) {
            isValid = false;
        }
        if (objectResult.getName().endsWith('Feed')) {
            isValid = false;
        }
        if (skippedObjects.contains(objectResult.getName())) {
            isValid = false;
        }
        return isValid;
    }
}
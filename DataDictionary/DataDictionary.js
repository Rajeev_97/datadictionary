import { LightningElement} from 'lwc';
import getObjects from '@salesforce/apex/DataDictionaryGeneratorController.getObjects'
import generateCsvString from '@salesforce/apex/DataDictionaryGeneratorController.generateCsvString'

export default class DataDictionary extends LightningElement {
    allObjectNames = [];
    selectedObjectNames = ['Account', 'Contact', 'Opportunity', 'Case', 'Product2'];
    hasRendered = false;
    isLoading = true;
    error;
    disableDownload = true;

    renderedCallback() {
        if (this.hasRendered) {
            return;
        }
        this.hasRendered = true;
        this.getObjectsFromSchema();
    }

    errorCallback(error, stack) {
        this.error = error;
        console.log(' this.error ', this.error);
    }


    getObjectsFromSchema() {
        getObjects().then(result => { 
            for (var index in result) {
                var objectDetail = result[index];
                this.allObjectNames.push({
                    label: objectDetail.objectLabel, 
                    value: objectDetail.objectName
                });
            }
            this.checkDownloadButtonVisibility();
            this.isLoading = false;
        })
        .catch(error => {
            this.error = error;
            this.isLoading = false;
        });
    }

    handleChange(event) {
        this.selectedObjectNames = event.detail.value;
        this.checkDownloadButtonVisibility();
    }

    checkDownloadButtonVisibility() {
        if (this.selectedObjectNames && this.selectedObjectNames.length > 0) {
            this.disableDownload = false;
        } else {
            this.disableDownload = true;
        }
    }

    downloadAsCsvFile(event) {

        let fileName = 'SalesforceDataDictionary.csv';

        generateCsvString({listOfObjects : this.selectedObjectNames}).then(result => { 
            let downloadElement = document.createElement('a');
            downloadElement.href = 'data:text/csv;charset=utf-8,' + encodeURIComponent(result);
            downloadElement.target = '_blank';
            downloadElement.download = fileName;
            document.body.appendChild(downloadElement);
            downloadElement.click();
        });
    }

}